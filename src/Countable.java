/** 	Murach, J. ( 2011).  Murach’s Java Programming, Training and 
        Reference, 4th Edition,  Fresno, CA: Mike Murach & Associates, Inc.
		Modifications by Wm Bowers, 2012
		Additional modifications by N.See, 2013
        Countable interface
**/

public interface Countable {
    
    void incrementCount();
    void resetCount();
    int getCount();
    String getCountString();
    
}
