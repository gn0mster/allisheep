/** 	Murach, J. ( 2011).  Murach’s Java Programming, Training and 
        Reference, 4th Edition,  Fresno, CA: Mike Murach & Associates, Inc.
		Modifications by Wm Bowers, 2012
		Additional modifications by N.See, 2013
        Alligator class
**/

public class Alligator implements Countable {
    
    private int count;
    
    public Alligator(){
        
        count = 1;
    }
    
    @Override
    public String getCountString() {
        return  ("  " + count + " alligator");
    }
    
    public void incrementCount(){
        count++; 
    }
    public void resetCount(){
        count = 1;
    }
    public int getCount(){
        return count;
    }
    
}
