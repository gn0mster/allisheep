
/**	BowersDate.java (c) 2000 - 2012 Wm Bowers All Rights Reserved.
	This application may be used with my permission provided:  1)
	this copyright notice remains in place and unaltered, and  2)
	the next user adds their modification notice to the existing
	audit trail.  3).  Permission to use automatically ends with
	at the completion of this course.
	Additional modifications by N.See, 2012
*/

import java.text.*;	// for Date
import java.util.*;	// for Date

public class SeeDate	{

	static Date now = new Date();

	public static void main(String [] args)	{
		System.out.printf("\n\n  Naomi See\n");
		System.out.printf("  Testing My Personal Date Class\n  %tB %te, %tY\n", now, now, now);
		verbose();
		printfDate();
		printfTime();
          System.out.printf("\n  Press the Enter key to continue.\n\n");
	}	// end main(...)

	public static void printfDate()	{
		System.out.printf("  %tA, %tB %te, %tY", now, now, now, now);
	}	// end getPrintfDate() method

	public static void printfTime()	{
		System.out.printf("  %tl:%tM:%tS%tp", now, now, now, now);
	}	// end getPrintfDate() method

	public static void verbose()	{
		System.out.printf("\n  The personal Date class uses printf(...) statements exclusively\n  to include printfDate( ) and printfTime( ) method calls to display the\n  computer's date and time on demand.\n\n  From your applications, you call these methods as follows, where my\n  last name would be replaced by your last name.  Viz:\n\n  SeeDate.printfDate();\n  SeeDate.printfTime();\n\n");
	}	// end verbose() method

}	// end SeeDate Class