
/**	BowersHeading.java (c) 2000 - 2012 Wm Bowers All Rights Reserved.
	This application may be used with my permission provided:  1)
	this copyright notice remains in place and unaltered, and  2)
	the next user adds their modification notice to the existing
	audit trail.  3).  Permission to use automatically ends with
	at the completion of this course.
	Additional modifications by N.See, 2012
*/

import java.util.*;	// for Date class
import java.text.*;	// for Date & DateFormat class

public class SeeHeading	{

	private static String firstName = "Naomi";
	private static String lastName = "See";
	private static String assignment;
    
    public SeeHeading() {}
	public static void main(String [] args)	{
		getHeading("Testing My Personal Heading Class");
		verbose();
	}	// end main

	public static void getHeading(String x)	{
		String fromAssignment = x;
		System.out.println("\n\n  " + getName());
		System.out.println("  " + fromAssignment);
		System.out.println("  " + getDate() + "\n");
	}	// end getHeading()

	public static String getName()	{
		return getFirstName() + " " + getLastName();
	}

	public static String getFirstName()	{
		return firstName;
	}	// end getFirstName()

	public static String getLastName()	{
		return lastName;
	}	// end getLastName()

	public static String getDate()	{
		String date;
		Date now = new Date();
		DateFormat longDate = DateFormat.getDateInstance(DateFormat.LONG);
		return date = longDate.format(now);
	}	// end getDate()

	public static String getTime()	{
		String time;
		Date nowTime = new Date();
		DateFormat longTime = DateFormat.getTimeInstance(DateFormat.SHORT);
		return time = longTime.format(nowTime);
	}	// end getTime()

	public static void setAssignment(String x)
	{	assignment = x;	}

	public static String getAssignment()
	{	return assignment;	}

	public static void verbose()	{
		System.out.println("\n  Your personal Heading class uses println(...) statements exclusively.\n\n  Except for testing this class's main(...), the getHeading() method\n  will be used in every assignment to receive information from other\n  applications.  All other methods will be used as needed throughout\n  the quarter.\n\n  The computer's getDate() and getTime() methods demonstrate alternative\n  ways of coding quite differently from that used in your personal Date\n  class.  You will need to know both ways of writing code.\n\n  We will be using calls to your personal heading class in every\n  assignment for the balance of the Quarter.\n\n  " + getDate() + " " + getTime() + "\n  Press the Enter key to continue.\n");
	}

}	// end SeeHeading

