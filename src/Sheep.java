/** 	Murach, J. ( 2011).  Murach’s Java Programming, Training and 
        Reference, 4th Edition,  Fresno, CA: Mike Murach & Associates, Inc.
		Modifications by Wm Bowers, 2012
		Additional modifications by N.See, 2013
        Sheep class
**/

import java.lang.*;

public class Sheep implements Countable, Cloneable {
    
    private int count;
    private String name;
    
    public Sheep(){
        
        count = 1;
    }
    
    
    public String getCountString() {
        return  ("  " + count + name);
    }
    
    public void incrementCount(){
        count++; 
    }
    
    public void resetCount(){
        count = 1;
    }

    public int getCount(){
        return count;
    }
    
    public void setName(String name) {
        
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        
        return super.clone();
    }
    
}
