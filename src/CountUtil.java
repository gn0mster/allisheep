/** 	Murach, J. ( 2011).  Murach’s Java Programming, Training and 
        Reference, 4th Edition,  Fresno, CA: Mike Murach & Associates, Inc.
		Modifications by Wm Bowers, 2012
		Additional modifications by N.See, 2013
        CountUtil class
**/
public class CountUtil {
    
    public static void count(Countable c, int maxCount) {
       c.resetCount();
        
       while(c.getCount() <= maxCount){
           System.out.println(c.getCountString());
           c.incrementCount(); 
       }
    }
    
}
