/** 	Murach, J. ( 2011).  Murach’s Java Programming, Training and 
        Reference, 4th Edition,  Fresno, CA: Mike Murach & Associates, Inc.
		Modifications by Wm Bowers, 2012
		Additional modifications by N.See, 2013
        CountTestApp class
**/

import java.util.*;
import java.lang.*;


public class CountTestApp {
    
    public static void main(String args[] ) {
        
        SeeHeading.getHeading("Assignment 6");
        countAlligators();
        countSheep();
        countMyself();
        endItAll();
    }
    
    public static void countAlligators() {
        
        int count = 4;
        
        System.out.println("  Counting alligators... I see " + count);
        
        Alligator a = new Alligator();
      
        CountUtil.count(a, count);
        System.out.println();
        
    }
    
    public static void countSheep() {
        int count1 = 1;
        int count2 = 2;
        int count3 = 3;
        
        System.out.println("  Counting sheep... I see " + count1);
        
        Sheep s1 = new Sheep();
        s1.setName(" BahRamEwe");
        CountUtil.count(s1, count1);
        System.out.println();
        
        Sheep s2 = null;
        Sheep s3 = null;
        
        try {
            s2 = (Sheep) s1.clone();
            s3 = (Sheep) s1.clone();
        } // end try
        catch (CloneNotSupportedException e) {
            System.out.println("\n  BahRamEwe did not get cloned!\n\n");
            e.printStackTrace();
        } // end catch
           
        s2.setName(" Dolly");
        s3.setName(" Grace");
        
        System.out.println("  I successfully cloned" + s1.getName() + "\n  and now there's a" + s2.getName() + " and a" + s3.getName() +"...\n");
        
        System.out.println("  Counting sheep... I see " + count3);
        
        CountUtil.count(s1, count1);
        CountUtil.count(s2, count1);
        CountUtil.count(s3, count1);
        System.out.println();
        System.out.print("  Cloning alligators? Ahhhh!!! What a scary thought!\n\n");
        
    }
    
    public static void countMyself() {
        
        int count = 1;
        
        Myself m1 = new Myself();
        m1.setName(" Naomi");
        
        Myself m2 = null;
        Myself m3 = null;
        
        try {
           m2 = (Myself) m1.clone();
           m3 = (Myself) m1.clone();
        } // end try
        catch (CloneNotSupportedException e) {
            System.out.println("\n  I was unable to clone myself!\n\n");
            e.printStackTrace();
        } // end catch
        
        m2.setName(" n0mi");
        m3.setName(" gn0mster");
        
        System.out.println("  Counting myself...");
        CountUtil.count(m1, count);
        
        System.out.println("  I successfully cloned" + m1.getName() + " twice, and \n  now there's a" + m2.getName() + " and a" + m3.getName() +"...\n");
        
        System.out.println("  Counting me, myself, and I...");
        
        CountUtil.count(m1, count);
        CountUtil.count(m2, count);
        CountUtil.count(m3, count);
        System.out.println();
        System.out.println("  I'd rather have more of me and less alligators!\n");
    }
    
    public static void endItAll() {
        System.out.print("  Program ended:");
        SeeDate.printfDate();
        SeeDate.printfTime();
        System.out.println();
        
    }
    
        
}
